<?php

namespace App\Clients;

use App\Users\Report;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use League\Csv\Writer;
use Illuminate\Support\Collection;
use SplTempFileObject;

class Client extends Eloquent
{
    protected $connection = 'mongodb';
    protected $guarded = [];
    protected $hidden = ['_id'];

    /**
     * Match filters plus the current project.
     *
     * @param array $filterList
     * @param integer $limit
     * @return $this
     */
    public function getFiltered(array $filterList, $limit = null)
    {
        $filterList = collect($filterList);

        return $this->raw(function ($collection) use ($filterList, $limit) {
            $filters = [];

            // Direct matches
            foreach ($filterList->except('service_id', 'booking_id', 'projectId', 'dateFrom', 'dateTo') as $filter => $value) {
                $filters[$filter] = $value;
            }

            // Project
            $filters['projects'] = ['$elemMatch' => ['id' => $this->getProjectId($filterList)]];

            // Date Range
            if (isset($filterList['dateFrom']) && isset($filterList['dateTo'])) {
                $filters['projects']['$elemMatch']['joined_at'] = ['$gt' => $filterList['dateFrom'], '$lt' => $filterList['dateTo']];
            }

            // Service
            if (isset($filterList['service_id'])) {
                $filters['services'] = ['$elemMatch' => ['id' => $filterList['service_id']]];
            }

            // Booking
            if (isset($filterList['booking_id'])) {
                $filters['bookings'] = ['$elemMatch' => ['id' => $filterList['booking_id']]];
            }

            return $collection->find(
                $filters,
                ['limit' => $limit]
            );
        });
    }

    private function getProjectId(Collection $filterList)
    {
        if (isset($filterList['projectId'])) {
            return $filterList['projectId'];
        }

        return current_project()->id;
    }

    /**
     * Order the parameters to achieve consistency when creating the CSV file.
     *
     * @return array
     */
    public function toArray()
    {
        $parameters = parent::toArray();
        ksort($parameters);
        return $parameters;
    }

    /**
     * Create the report and return the path.
     *
     * @param Report $report
     * @throws \League\Csv\CannotInsertRecord
     * @return string
     */
    public function createReport(Report $report)
    {
        $fields = (array)$report->columns_list;

        // Order the parameters to achieve consistency when creating the CSV file.
        sort($fields);

        $data = $this->prepareDataForReport($report);

        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne($fields);
        $csv->insertAll($data);

        $fileName = $this->generateReportName();
        Storage::disk('local')->put($fileName, $csv->getContent());

        return base_path('storage/app/'.$fileName);
    }

    /**
     * @param Report $report
     * @return mixed
     */
    private function prepareDataForReport(Report $report)
    {
        $clients = $this->getFiltered(
            (array) $report->filters_list
        )->toArray();

        $columns = array_fill_keys((array)$report->columns_list, '');

        foreach($clients as &$client) {
            $client = array_intersect_key($client, $columns);

            // In case there are empty values.
            $client = array_merge($columns, $client);

            ksort($client);
        }

        return $clients;
    }

    private function generateReportName()
    {
        return 'report-' . time() . '.csv';
    }

    /**
     * Return the client projects.
     *
     * @param string $clientCrmId
     * @return array
     */
    public static function getProjects($clientCrmId)
    {
        $client = Client::where('id', $clientCrmId)->first();

        if (!$client) {
            return [];
        }

        return array_wrap($client->projects);
    }

    /**
     * Return the client services.
     *
     * @param string $clientCrmId
     * @return array
     */
    public static function getServices($clientCrmId)
    {
        $client = Client::where('id', $clientCrmId)->first();

        if (!$client) {
            return [];
        }

        return array_wrap($client->services);
    }

    /**
     * Return the client bookings.
     *
     * @param string $clientCrmId
     * @return array
     */
    public static function getBookings($clientCrmId)
    {
        $client = Client::where('id', $clientCrmId)->first();

        if (!$client) {
            return [];
        }

        return array_wrap($client->bookings);
    }
}
